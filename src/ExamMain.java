

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class ExamMain {

	public static void main(String[] args) {
		String filename = "Homework.txt";
		ArrayList<Homework> homeworkList = new ArrayList<Homework>();
		ArrayList<Exam> examList = new ArrayList<Exam>();
		
		FileReader fileReader = null;
		
		try {
			fileReader = new FileReader(filename);
			BufferedReader buff = new BufferedReader(fileReader);
			String line;
			for (line = buff.readLine(); line != null; line = buff.readLine()){
				String[] data = line.split(", ");
				String name = data[0].trim();
				Double s1 = Double.parseDouble(data[1].trim());
				Double s2 = Double.parseDouble(data[2].trim());
				Double s3 = Double.parseDouble(data[3].trim());
				Double s4 = Double.parseDouble(data[4].trim());
				Double s5 = Double.parseDouble(data[5].trim());
				double sum = s1+s2+s3+s4+s5;
				
				Homework hw = new Homework(name);
				hw.setScore(sum);
				homeworkList.add(hw);
				
			}
			
			filename = "exam.txt";
			fileReader = new FileReader(filename);
			buff = new BufferedReader(fileReader);
			for (line = buff.readLine(); line != null; line = buff.readLine()){
				String[] data = line.split(", ");
				Double n1 = Double.parseDouble(data[1].trim());
				Double n2 = Double.parseDouble(data[2].trim());
				
				Exam ex = new Exam(data[0].trim());
				
				ex.setScore(n1+n2);
				
				examList.add(ex);
				
			}
			
			System.out.println("\tHomework Score Average ");
			
			for(Homework h:homeworkList){
				System.out.println(h.getName()+"\t:\t"+h.getScore());				
			}
			
			
			System.out.println("\tExam Score Average ");
			for(Exam e:examList){
				System.out.println(e.getName()+"\t:\t"+e.getScore());				
			}
			
			
			FileWriter fileWriter = null;
			fileWriter = new FileWriter("average.txt",true);
			BufferedWriter out = new BufferedWriter(fileWriter);

			for(Exam e:examList){
				out.write(e.getName()+", "+e.getScore());
				out.newLine();
			}
			out.flush();
		
		}
		catch (FileNotFoundException e){
			System.err.println("File Not Found : "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading file");
		}
		finally {
			try {
				if (fileReader != null)
						fileReader.close();
			} catch (IOException e) {
				System.err.println("Can not close file.");
			}
		}
	
	}
	
}
