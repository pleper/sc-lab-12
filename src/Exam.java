
public class Exam {
	private String name;
	private double score;

	public Exam(String name){
		this.name = name;
		this.score = 0;
	}
	
	public void setScore(double score){
		this.score += score;
	}
	
	public double getScore(){
		return score/2.0;
	}
	
	public String getName(){
		return name;
	}
}
