
public class PhoneBook {
	private String name;
	private String num;
	
	public PhoneBook(String name, String num){
		this.name = name;
		this.num = num;	
	}
	
	public String toString(){
		return name+"\t:\t"+num;
	}
}
