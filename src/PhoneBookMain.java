
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class PhoneBookMain {

	public static void main(String[] args) {
		String fileName = "PhoneBook.txt";
		ArrayList<PhoneBook> phoneList = new ArrayList<PhoneBook>();

		try {

			FileReader fileReader = new FileReader(fileName);
			BufferedReader buff = new BufferedReader(fileReader);
			String line;

			for (line = buff.readLine(); line != null; line = buff.readLine()) {
				String[] in = line.split(", ");
				String name = in[0].trim();
				String number = in[1].trim();

				PhoneBook phone = new PhoneBook(name, number);
				phoneList.add(phone);
			}
			System.out
					.println("\tPhone Book List \n-------------------------- ");
			for (PhoneBook p : phoneList) {
				System.out.println(p);
			}

		} catch (FileNotFoundException e) {
			System.err.println("File Not Found : " + fileName);
		} catch (IOException e) {
			System.err.println("Error reading file");
		}

	}

}
