

public class Homework {
	
	private String name;
	private double score;

	public Homework(String name){
		this.name = name;
		this.score = 0;
	}
	
	public void setScore(double score){
		this.score += score;
	}
	
	public double getScore(){
		return score/5.0;
	}
	
	public String getName(){
		return name;
	}
}
