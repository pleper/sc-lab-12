
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class HomeworkMain {

	public static void main(String[] args) {
		String filename = "Homework.txt";

		ArrayList<Homework> homeList = new ArrayList<Homework>();

		FileReader fileReader = null;
		try {
			fileReader = new FileReader(filename);
			BufferedReader buff = new BufferedReader(fileReader);
			String line;
			for (line = buff.readLine(); line != null; line = buff.readLine()) {
				String[] data = line.split(", ");
				Double n1 = Double.parseDouble(data[1].trim());
				Double n2 = Double.parseDouble(data[2].trim());
				Double n3 = Double.parseDouble(data[3].trim());
				Double n4 = Double.parseDouble(data[4].trim());
				Double n5 = Double.parseDouble(data[5].trim());

				Homework hw = new Homework(data[0].trim());

				hw.setScore(n1 + n2 + n3 + n4 + n5);

				homeList.add(hw);

			}

			FileWriter fileWriter = null;
			fileWriter = new FileWriter("average.txt");
			BufferedWriter out = new BufferedWriter(fileWriter);

			System.out.println("\tHomework Score Average");

			for (Homework h : homeList) {
				System.out.println(h.getName() + "\t:\t" + h.getScore());
				out.write(h.getName() + ", " + h.getScore());
				out.newLine();
			}

			out.flush();

		} catch (FileNotFoundException e) {
			System.err.println("File Not Found : " + filename);
		} catch (IOException e) {
			System.err.println("Error reading file");
		} finally {
			try {
				if (fileReader != null)
					fileReader.close();
			} catch (IOException e) {
				System.err.println("Can not close file.");
			}
		}

	}

}
